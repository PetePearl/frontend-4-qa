import '@/app/assets/styles/main.css'
import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import { Provider } from 'react-redux'

import { store } from '@/app/store'
import { MainPage } from '@/pages/MainPage'
import { InitStoreProvider } from '@/app/providers/InitStoreProvider'

export const App: React.FC = () => (
  <Provider store={store}>
    <BrowserRouter>
      <Routes>
        <Route element={<InitStoreProvider />}>
          <Route element={<MainPage />} path="/" />
          <Route element={<MainPage />} path="/about" />
        </Route>
      </Routes>
    </BrowserRouter>
  </Provider>
)
