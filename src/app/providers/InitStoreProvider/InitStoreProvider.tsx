import { FC, useEffect } from 'react'
import { Outlet } from 'react-router-dom'

import { useAppDispatch } from '@/shared/hooks'
import { getWeatherThunk } from '@/entities/weather'

export const InitStoreProvider: FC = () => {
  const dispatch = useAppDispatch()
  useEffect(() => {
    dispatch(getWeatherThunk())
  }, [dispatch])

  return <Outlet />
}
