import { configureStore } from '@reduxjs/toolkit'

import { weatherSlice } from '@/entities/weather/model'

export const store = configureStore({
  reducer: {
    [weatherSlice.name]: weatherSlice.reducer,
  },
})
