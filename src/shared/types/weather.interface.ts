import { TTime } from '@/shared/types/time.type'

export interface IWeather {
  latitude?: number,
  longitude?: number,
  hourly: {
    time?: TTime[],
    temperature_2m?: number[]
  }
}
