import { store } from '@/app/store'

export type TAppStore = ReturnType<typeof store.getState>
