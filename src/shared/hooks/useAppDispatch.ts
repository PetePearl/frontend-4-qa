import { useDispatch } from 'react-redux'

import { store } from '@/app/store'

export const useAppDispatch = () => useDispatch<typeof store.dispatch>()
