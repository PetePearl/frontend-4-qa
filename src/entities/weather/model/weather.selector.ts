import { IProfile } from '@/shared/types/profile.interface'
import { TAppStore } from '@/shared/types/appStore.type'

export const weatherSelector = (store: TAppStore): IProfile => store.profile
