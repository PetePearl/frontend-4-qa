import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'

import { weatherService } from '@/entities/weather'
import { TAppStore } from '@/shared/types/appStore.type'
import { IWeather } from '@/shared/types/weather.interface'
import { TTime } from '@/shared/types/time.type'

const initialState: IWeather = {
  hourly: {
    time: [],
    temperature_2m: [],
  },
}

export const getWeatherThunk = createAsyncThunk(
  'weatherThunk',
  weatherService.getWeather,
)

export const weatherSlice = createSlice({
  name: 'weather',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(
        getWeatherThunk.fulfilled,
        (_, action) => action.payload,
      )
  },
})

export const getWeatherDataSelector = (store: { weather: IWeather }): IWeather => store.weather
export const getWeatherTSelector = (store: TAppStore): [ TTime, number ][] => {
  const times = store.weather.hourly.time || []
  const temperatures = store.weather.hourly.temperature_2m || []

  return times.reduce<[ TTime, number ][]>((
    acc,
    el,
    index,
  ) => [...acc, [el.slice(-5), temperatures[index]]], [])
}
