import { IWeather } from '@/shared/types/weather.interface'

interface IProfileService {
  getWeather: () => Promise<IWeather>
}

export const weatherService: IProfileService = {
  async getWeather() {
    return fetch(
      'https://api.open-meteo.com/v1/forecast?latitude=52.52&longitude=13.41&hourly=temperature_2m',
    )
      .then((data) => data.json())
  },
}
