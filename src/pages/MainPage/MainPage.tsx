import { FC } from 'react'
import cn from 'classnames'
import { useSelector } from 'react-redux'

import { getWeatherTSelector } from '@/entities/weather'

import s from './MainPage.module.css'

const Graph: FC = () => {
  const dataTemp = useSelector(getWeatherTSelector)

  return (
    <table>
      <thead>
        <tr>
          <th>Время</th>
          <th>Температура</th>
        </tr>
      </thead>
      {dataTemp.map(([time, temperature]) => (
        <tr>
          <td>{time}</td>
          <td>{temperature}</td>
        </tr>
      ))}
    </table>
  )
}

export const MainPage: FC = () => <div className={cn(s.aside, s.main)}><Graph /></div>
