import path from 'path'
import * as url from 'url'

import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import tsconfigPaths from 'vite-tsconfig-paths'
import { VitePWA } from 'vite-plugin-pwa'
// todo: вынести в отдельный файл
const vitePWA = VitePWA({
  registerType: 'autoUpdate',
  includeAssets: ['favicon.ico', 'android-chrome-192x192.png', 'android-chrome-512x512.png'],
  workbox: {
    clientsClaim: true,
    skipWaiting: true,
    globIgnores: [
      'sw.js',
      'assets',
    ],
    swDest: path.resolve(path.dirname(url.fileURLToPath(import.meta.url)), 'sw.js'),
  },

  manifest: {
    name: 'моизадачи',
    short_name: 'моизадачи',
    description: 'Лучший проект',
    background_color: '#FFFFFF',
    theme_color: '#FFFFFF',
    orientation: 'natural',
    display: 'standalone',
    display_override: ['window-controls-overlay'],
    lang: 'ru-RU',
    start_url: '/auth',
    dir: 'ltr',
    categories: ['Задачи', 'Проекты'],
    handle_links: 'preferred',
    shortcuts: [
      {
        description: 'Авторизация',
        name: 'Авторизация',
        url: '/auth/',
      },
      {
        description: 'Главная',
        name: 'Главная',
        url: '/',
      },
    ],
    icons: [
      {
        src: '/manifest-icons/android-chrome-192x192.png',
        sizes: '192x192',
        type: 'image/png',
      },
      {
        src: '/manifest-icons/android-chrome-512x512.png',
        sizes: '512x512',
        type: 'image/png',
      },
    ],
  },
})

export default defineConfig({
  plugins: [react(), tsconfigPaths(), vitePWA],
  server: {
    port: 3000,
    proxy: {
      '/api': 'http://localhost:3002',
    },
  },
  preview: {
    port: 3000,
  },
  build: {
    assetsInlineLimit: 0,
  },
})
